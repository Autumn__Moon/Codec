# Codec

#### 介绍
这是一个openHarmony字符转换工具。

#### 软件架构
使用Stage模型开发。


#### 安装教程

1.  电脑内安装HDC；
2.  连接openHarmony设备；
3.  打开cmd，输入 "hdc install hap包路径"，敲回车后等待发送文件并安装成功。

    注意：hap包路径不支持中文，请以英文名，英文标点为路径。

#### 使用说明

1.  下载HUAWEI DevEco Studio 3.1 Release [下载地址](https://contentcenter-vali-drcn.dbankcdn.cn/pvt_2/DeveloperAlliance_package_901_9/16/v3/YO_7mAQNTbS8jekrvez5IA/devecostudio-windows-3.1.0.500.zip?HW-CC-KV=V1&HW-CC-Date=20230512T073650Z&HW-CC-Expire=315360000&HW-CC-Sign=90814E421B9A6D8DB4757FAFC21A965CF890A387DF9A2633B4AB797AD77E6485)；
2.  安装OHOS Full SDK 3.2 Release [下载地址](http://download.ci.openharmony.cn/version/Daily_Version/ohos-sdk-full/20230607_022207/version-Daily_Version-ohos-sdk-full-20230607_022207-ohos-sdk-full.tar.gz)；
3.  打开工程编译即可。